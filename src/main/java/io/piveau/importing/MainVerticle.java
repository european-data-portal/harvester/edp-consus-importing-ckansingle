package io.piveau.importing;

import io.piveau.importing.ckan.ImportingCkanSingleVerticle;
import io.piveau.pipe.connector.PipeConnector;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Launcher;
import io.vertx.core.Promise;

import java.util.Arrays;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.deployVerticle(ImportingCkanSingleVerticle.class, new DeploymentOptions().setWorker(true), result -> {
            if (result.succeeded()) {
                PipeConnector.create(vertx, cr -> {
                    if (cr.succeeded()) {
                        cr.result().consumer(ImportingCkanSingleVerticle.ADDRESS);
                        startPromise.complete();
                    } else {
                        startPromise.fail(cr.cause());
                    }
                });
            } else {
                startPromise.fail(result.cause());
            }
        });
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

}
