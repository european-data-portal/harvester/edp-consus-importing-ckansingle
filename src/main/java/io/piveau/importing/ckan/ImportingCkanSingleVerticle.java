package io.piveau.importing.ckan;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.importing.ckan.response.CkanArrayResult;
import io.piveau.importing.ckan.response.CkanError;
import io.piveau.importing.ckan.response.CkanResponse;
import io.piveau.pipe.PipeContext;
import io.piveau.utils.JenaUtils;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.RDF;

import java.util.ArrayList;
import java.util.List;

public class ImportingCkanSingleVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.importing.ckansingle.queue";

    private WebClient client;

    private int defaultDelay;
    private int pageSize;

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);
        client = WebClient.create(vertx);
        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray()
                        .add("PIVEAU_IMPORTING_SEND_LIST_DELAY")
                        .add("PIVEAU_PAGE_SIZE")));
        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(envStoreOptions));
        retriever.getConfig(ar -> {
            if (ar.succeeded()) {
                defaultDelay = ar.result().getInteger("PIVEAU_IMPORTING_SEND_LIST_DELAY", 8000);
                pageSize = ar.result().getInteger("PIVEAU_PAGE_SIZE", 1000);
                startPromise.complete();
            } else {
                startPromise.fail(ar.cause());
            }
        });
        retriever.listen(change -> defaultDelay = change.getNewConfiguration().getInteger("PIVEAU_IMPORTING_SEND_LIST_DELAY", 8000));
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        JsonObject config = pipeContext.getConfig();
        pipeContext.log().info("Import started.");

        String address = config.getString("address");
        HttpRequest<Buffer> request = client.getAbs(address + "/api/action/package_list")
                .setQueryParam("limit", String.valueOf(pageSize))
                .expect(ResponsePredicate.SC_OK);

        fetchPage(request, 0, new ArrayList<>(), pipeContext);
    }

    private void fetchPage(HttpRequest<Buffer> request, int offset, List<String> identifiers, PipeContext pipeContext) {
        request.setQueryParam("offset", String.valueOf(offset)).send(ar -> {
            if (ar.succeeded()) {
                CkanResponse ckanResponse = new CkanResponse(ar.result().bodyAsJsonObject());
                if (ckanResponse.isError()) {
                    CkanError ckanError = ckanResponse.getError();
                    pipeContext.setFailure(ckanError.getMessage());
                } else {
                    CkanArrayResult ckanResult = ckanResponse.getArrayResult();
                    JsonArray result = ckanResult.getContent();
                    result.forEach(name -> fetchDataset(name.toString(), -1, identifiers, pipeContext));
                    if (!result.isEmpty()) {
                        fetchPage(request, offset + pageSize, identifiers, pipeContext);
                    } else {
                        pipeContext.log().info("Import metadata finished");
                        int delay = pipeContext.getConfig().getInteger("sendListDelay", defaultDelay);
                        vertx.setTimer(delay, t -> {
                            ObjectNode info = new ObjectMapper().createObjectNode()
                                    .put("content", "identifierList")
                                    .put("catalogue", pipeContext.getConfig().getString("catalogue"));
                            pipeContext.setResult(new JsonArray(identifiers).encodePrettily(), "application/json", info).forward();
                        });
                    }
                }
            } else {
                pipeContext.setFailure(ar.cause());
            }
        });
    }

    private void fetchDataset(String name, int total, List<String> identifiers, PipeContext pipeContext) {
        identifiers.add(name);
        JsonObject config = pipeContext.getConfig();
        String address = config.getString("address");
        boolean sendHash = config.getBoolean("sendHash", false);
        String outputFormat = config.getString("outputFormat", "application/n-triples");
        ObjectNode dataInfo = new ObjectMapper().createObjectNode()
                .put("total", total)
                .put("counter", identifiers.size())
                .put("identifier", name)
                .put("catalogue", config.getString("catalogue"));
        HttpRequest<Buffer> request = client.getAbs(address + "/dataset/" + name + ".rdf").expect(ResponsePredicate.SC_OK);
        request.send(ar -> {
            if (ar.succeeded()) {
                String dataset = ar.result().bodyAsString();
                Model model = JenaUtils.read(dataset.getBytes(), "application/rdf+xml", address);
                ResIterator it = model.listResourcesWithProperty(RDF.type, DCAT.Dataset);
                if (it.hasNext()) {
                    Resource res = it.next();
                    Model extracted = JenaUtils.extractResource(res);
                    if (sendHash) {
                        dataInfo.put("hash", JenaUtils.canonicalHash(extracted));
                    }
                    pipeContext.setResult(JenaUtils.write(extracted, outputFormat), outputFormat, dataInfo).forward();
                    pipeContext.log().info("Data imported: {}", dataInfo);
                } else {
                    pipeContext.log().error("Extracting resource: {}", dataInfo);
                }
            } else {
                pipeContext.log().error("Fetching dataset " + name, ar.cause());
            }
        });
    }

}
